package main

import (
	"fmt"
	"net"
	"os"

	"golang.org/x/net/ipv4"
	"golang.org/x/sys/unix"
)

// createRawSocket creates a raw socket bound to the specified network interface
func createRawSocket(iface *net.Interface) (*ipv4.RawConn, error) {
	s, err := unix.Socket(unix.AF_INET, unix.SOCK_RAW, unix.IPPROTO_UDP)
	if err != nil {
		return nil, fmt.Errorf("error creating socket: %w", err)
	}

	// Ensure the socket is closed on error
	defer func() {
		if err != nil {
			unix.Close(s)
		}
	}()

	// Set SO_REUSEPORT (required for binding to the same port as another socket)
	if err = unix.SetsockoptInt(s, unix.SOL_SOCKET, unix.SO_REUSEPORT, 1); err != nil {
		return nil, fmt.Errorf("error setting SO_REUSEPORT: %w", err)
	}

	// Set SO_BROADCAST (required for sending broadcast packets)
	if err = unix.SetsockoptInt(s, unix.SOL_SOCKET, unix.SO_BROADCAST, 1); err != nil {
		return nil, fmt.Errorf("error setting SO_BROADCAST: %w", err)
	}

	// Bind to the specific network interface
	if err = unix.BindToDevice(s, iface.Name); err != nil {
		return nil, fmt.Errorf("error binding to device: %w", err)
	}

	// Convert to net.PacketConn
	f := os.NewFile(uintptr(s), "")
	defer f.Close()

	c, err := net.FilePacketConn(f)
	if err != nil {
		return nil, fmt.Errorf("error creating packet conn: %w", err)
	}

	// Wrap with ipv4.RawConn
	rawConn, err := ipv4.NewRawConn(c)
	if err != nil {
		return nil, fmt.Errorf("error creating raw conn: %w", err)
	}

	return rawConn, nil
}

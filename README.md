# DHCP Client Tool

## Background

This DHCP client tool is an educational project that demonstrates the DHCP process in action. It allows users to observe the DHCP handshake, including DISCOVER, OFFER, REQUEST, and ACK messages, and displays information about the DHCP options received from the server.

DHCP is a network management protocol used to dynamically assign an IP address to any device, or node, on a network so it can communicate using IP. This tool shows how this process works at a basic level.

## Features

- Implements a DHCP client in Go (written from scratch).
- Allows users to observe the DHCP handshake, including DISCOVER, OFFER, REQUEST, and ACK messages.
- Allows the user to request custom DHCP options.
- Shows DHCP options received from the server; so far everything from [RFC 2132](https://www.rfc-editor.org/rfc/rfc2132). Other options might be implemented in future if I feel like reading RFCs on a rainy day.
- Supports using a custom MAC address or generating a random MAC address.
- Allows repeating the DHCP request process multiple times. Not for DoS attacks.

## Requirements

- Go 1.23 or higher
- Currently **only works on Linux**, due to certain raw socket requirements
- Root privileges (for creating raw sockets)

## Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/mblows/dhcp-tool.git
cd dhcp-tool
```

2. Build the project (assuming you have the [Go compiler installed](https://go.dev/dl/)):

```bash
go build .
```

This will create an executable named `dhcp-tool` in your current directory.

## Usage

To use the tool, you need to run it with root privileges and specify the network interface you want to use:

```bash
sudo ./dhcp-tool -i <interface_name>
```

### Available Options

- `-i <interface_name>`: Specify the network interface to use (required)
- `-o <options>`: Request custom DHCP options (comma-separated list of option numbers)
- `-m <mac_address>`: Use a custom MAC address for the DHCP request
- `-r`: Generate a random MAC address for the DHCP request
- `-n <number>`: Repeat the DHCP request process the specified number of times
- `-l`: List available network interfaces

### Examples

To request custom DHCP options:

```bash
sudo ./dhcp-tool -i <interface_name> -o 1,3,6
```

To use a custom MAC address:

```bash
sudo ./dhcp-tool -i <interface_name> -m 00:11:22:33:44:55
```

To use a random MAC address:

```bash
sudo ./dhcp-tool -i <interface_name> -r
```

To repeat the DHCP request process 3 times:

```bash
sudo ./dhcp-tool -i <interface_name> -n 3
```

To list available network interfaces:

```bash
sudo ./dhcp-tool -l
```

Replace `<interface_name>` with the name of your network interface (e.g., `eth0`, `wlan0`).

## Example Output

```
./dhcp-tool -l
Name       MAC Address       Status
lo                           up
eth0       00:11:22:33:44:55 up
wlan0      de:ad:be:ee:ff:01 up
docker0    ca:fe:00:00:11:22 down

./dhcp-tool -i eth0
Broadcasting DHCP discovery message on eth0 (00:11:22:33:44:55)...
Received DHCP offer of 192.168.0.1 from 192.168.0.254
Server DHCP Options:
    DHCP Message Type (53): DHCPOFFER (2)
    Subnet Mask (1): 255.255.255.0
    Router (3): 192.168.0.254
    DNS Server (6): 192.168.0.254
    IP Address Lease Time (51): 30m0s
    Server Identifier (54): 192.168.0.254

Asking 192.168.0.254 to lease 192.168.0.1...
Received DHCP acknowledgement. Confirmed IP: 192.168.0.1
Server DHCP Options:
    DHCP Message Type (53): DHCPACK (5)
    Subnet Mask (1): 255.255.255.0
    Router (3): 192.168.0.254
    DNS Server (6): 192.168.0.254
    IP Address Lease Time (51): 30m0s
    Server Identifier (54): 192.168.0.254
```

## Note

This tool is designed for educational purposes, was coded very quickly, and probably contains numerous bugs.

## License

This project is open source and available under the [Unlicense](https://unlicense.org/).

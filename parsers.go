// parsers.go

package main

import (
	"encoding/binary"
	"fmt"
	"net"
	"time"
)

// DHCPOptions is a map of DHCP option codes to their information
// See https://www.iana.org/assignments/bootp-dhcp-parameters/bootp-dhcp-parameters.xhtml
var DHCPOptions = map[uint8]DHCPOptionInfo{
	OptionPad:                                        {"Pad", 0, "Used to pad other options for alignment"},
	OptionSubnetMask:                                 {"Subnet Mask", 4, "Specifies the client's subnet mask"},
	OptionTimeOffset:                                 {"Time Offset", 4, "Offset from Coordinated Universal Time (UTC) in seconds"},
	OptionRouter:                                     {"Router(s)", 4, "List of available routers on the client's subnet"},
	OptionTimeServer:                                 {"Time Server(s)", 4, "List of RFC 868 time servers available to the client"},
	OptionNameServer:                                 {"Name Server(s)", 4, "List of IEN 116 name servers available to the client"},
	OptionDomainNameServer:                           {"DNS Server(s)", 4, "List of DNS servers available to the client"},
	OptionLogServer:                                  {"Log Server(s)", 4, "List of MIT-LCS UDP log servers available to the client"},
	OptionCookieServer:                               {"Cookie Server(s)", 4, "List of RFC 865 cookie servers available to the client"},
	OptionLPRServer:                                  {"LPR Server(s)", 4, "List of RFC 1179 line printer servers available to the client"},
	OptionImpressServer:                              {"Impress Server(s)", 4, "List of Imagen Impress servers available to the client"},
	OptionResourceLocationServer:                     {"Resource Location Server(s)", 4, "List of RFC 887 Resource Location servers available to the client"},
	OptionHostName:                                   {"Host Name", 1, "Name of the client"},
	OptionBootFileSize:                               {"Boot File Size", 2, "Length of the default boot image in 512-octet blocks"},
	OptionMeritDumpFile:                              {"Merit Dump File", 1, "Path where the client's core dump should be stored"},
	OptionDomainName:                                 {"Domain Name", 1, "The DNS domain name of the client"},
	OptionSwapServer:                                 {"Swap Server", 4, "IP address of the client's swap server"},
	OptionRootPath:                                   {"Root Path", 1, "Path name of the client's root disk"},
	OptionExtensionsPath:                             {"Extensions Path", 1, "Path name of a file containing additional options"},
	OptionIPForwarding:                               {"IP Forwarding Enable/Disable", 1, "Enable/disable IP forwarding on the client"},
	OptionNonLocalSourceRouting:                      {"Non-Local Source Routing Enable/Disable", 1, "Enable/disable forwarding of datagrams with non-local source routes"},
	OptionPolicyFilter:                               {"Policy Filter(s)", 8, "Policy filters for non-local source routing"},
	OptionMaximumDatagramReassemblySize:              {"Maximum Datagram Reassembly Size", 2, "Maximum size datagram that the client should be prepared to reassemble"},
	OptionDefaultIPTTL:                               {"Default IP Time-to-live", 1, "Default TTL that the client should use on outgoing datagrams"},
	OptionPathMTUAgingTimeout:                        {"Path MTU Aging Timeout", 4, "Timeout (in seconds) for Path MTU Discovery"},
	OptionPathMTUPlateauTable:                        {"Path MTU Plateau Table", 2, "Table of MTU sizes for Path MTU Discovery"},
	OptionInterfaceMTU:                               {"Interface MTU", 2, "MTU to use on this interface"},
	OptionAllSubnetsAreLocal:                         {"All Subnets are Local", 1, "Indicates if all subnets of the IP network use the same MTU"},
	OptionBroadcastAddress:                           {"Broadcast Address", 4, "Broadcast address in use on the client's subnet"},
	OptionPerformMaskDiscovery:                       {"Perform Mask Discovery", 1, "Specifies whether the client should perform subnet mask discovery using ICMP"},
	OptionMaskSupplier:                               {"Mask Supplier", 1, "Specifies whether the client should respond to subnet mask requests using ICMP"},
	OptionPerformRouterDiscovery:                     {"Perform Router Discovery", 1, "Specifies whether the client should solicit routers using Router Discovery"},
	OptionRouterSolicitationAddress:                  {"Router Solicitation Address", 4, "Address to which the client should transmit router solicitation requests"},
	OptionStaticRoute:                                {"Static Route(s)", 8, "List of static routes the client should install in its routing cache"},
	OptionTrailerEncapsulation:                       {"Trailer Encapsulation", 1, "Specifies whether the client should negotiate the use of trailers (RFC 893)"},
	OptionARPCacheTimeout:                            {"ARP Cache Timeout", 4, "Timeout in seconds for ARP cache entries"},
	OptionEthernetEncapsulation:                      {"Ethernet Encapsulation", 1, "Specifies whether the client should use Ethernet Version 2 or IEEE 802.3 encapsulation"},
	OptionTCPDefaultTTL:                              {"TCP Default TTL", 1, "Default TTL that the client should use when sending TCP segments"},
	OptionTCPKeepaliveInterval:                       {"TCP Keepalive Interval", 4, "Interval (in seconds) that the client TCP should wait before sending a keepalive message"},
	OptionTCPKeepaliveGarbage:                        {"TCP Keepalive Garbage", 1, "Specifies whether the client should send TCP keepalive messages with an octet of garbage"},
	OptionNISDomain:                                  {"Network Information Service Domain", 1, "Name of the client's NIS domain"},
	OptionNISServers:                                 {"Network Information Server(s)", 4, "List of NIS servers available to the client"},
	OptionNTPServers:                                 {"Network Time Protocol Server(s)", 4, "List of NTP servers available to the client"},
	OptionVendorSpecificInformation:                  {"Vendor Specific Information", 1, "Vendor-specific information"},
	OptionNetBIOSOverTCPIPNameServer:                 {"NetBIOS over TCP/IP Name Server(s)", 4, "List of RFC 1001/1002 NBNS name servers"},
	OptionNetBIOSOverTCPIPDatagramDistributionServer: {"NetBIOS over TCP/IP Datagram Distribution Server(s)", 4, "List of RFC 1001/1002 NBDD servers"},
	OptionNetBIOSOverTCPIPNodeType:                   {"NetBIOS over TCP/IP Node Type", 1, "NetBIOS node type (e.g., 1=B-node, 2=P-node, 4=M-node, 8=H-node)"},
	OptionNetBIOSOverTCPIPScope:                      {"NetBIOS over TCP/IP Scope", 1, "NetBIOS over TCP/IP scope"},
	OptionXWindowSystemFontServer:                    {"X Window System Font Server(s)", 4, "List of X Window System Font servers"},
	OptionXWindowSystemDisplayManager:                {"X Window System Display Manager(s)", 4, "List of X Window System Display Manager servers"},
	OptionIPAddressLeaseTime:                         {"IP Address Lease Time", 4, "Length of time in seconds for the IP address lease"},
	OptionOptionOverload:                             {"Option Overload", 1, "Indicates which fields in the DHCP message contain options"},
	OptionDHCPMessageType:                            {"DHCP Message Type", 1, "Type of DHCP message (e.g., 1=DHCPDISCOVER, 2=DHCPOFFER)"},
	OptionServerIdentifier:                           {"Server Identifier", 4, "IP address of the DHCP server sending the message"},
	OptionMessage:                                    {"Message", 1, "Error message in DHCPNAK or reason for declining in DHCPDECLINE"},
	OptionMaximumDHCPMessageSize:                     {"Maximum DHCP Message Size", 2, "Maximum length DHCP message that the client is willing to accept"},
	OptionRenewalTimeValue:                           {"Renewal (T1) Time Value", 4, "Time interval before the client enters the RENEWING state"},
	OptionRebindingTimeValue:                         {"Rebinding (T2) Time Value", 4, "Time interval before the client enters the REBINDING state"},
	OptionVendorClassIdentifier:                      {"Vendor Class Identifier", 1, "Identifies the vendor type and configuration of a DHCP client"},
	OptionNISPlusDomain:                              {"Network Information Service+ Domain", 1, "Name of the client's NIS+ domain"},
	OptionNISPlusServers:                             {"Network Information Service+ Server(s)", 4, "List of NIS+ servers available to the client"},
	OptionTFTPServerName:                             {"TFTP Server Name", 1, "TFTP server name"},
	OptionBootfileName:                               {"Bootfile Name", 1, "Bootfile name"},
	OptionMobileIPHomeAgent:                          {"Mobile IP Home Agent(s)", 0, "List of Mobile IP home agents available to the client"},
	OptionSMTPServer:                                 {"Simple Mail Transfer Protocol (SMTP) Server(s)", 4, "List of SMTP servers available to the client"},
	OptionPOP3Server:                                 {"Post Office Protocol (POP3) Server(s)", 4, "List of POP3 servers available to the client"},
	OptionNNTPServer:                                 {"Network News Transfer Protocol (NNTP) Server(s)", 4, "List of NNTP servers available to the client"},
	OptionDefaultWWWServer:                           {"Default World Wide Web (WWW) Server(s)", 4, "List of WWW servers available to the client"},
	OptionDefaultFingerServer:                        {"Default Finger Server(s)", 4, "List of Finger servers available to the client"},
	OptionDefaultIRCServer:                           {"Default Internet Relay Chat (IRC) Server(s)", 4, "List of IRC servers available to the client"},
	OptionStreetTalkServer:                           {"StreetTalk Server(s)", 4, "List of StreetTalk servers available to the client"},
	OptionStreetTalkDirectoryAssistanceServer:        {"StreetTalk Directory Assistance (STDA) Server(s)", 4, "List of STDA servers available to the client"},
	OptionEnd:                                        {"End", 0, "Marks the end of the DHCP options field"},
}

// OptionParser is a function type for parsing DHCP options
type OptionParser func(data []byte) (any, error)

// Map of option parsers
var optionParsers = map[uint8]OptionParser{
	OptionSubnetMask:                                 parseIPAddress,
	OptionTimeOffset:                                 parseInt32,
	OptionRouter:                                     parseIPAddressList,
	OptionTimeServer:                                 parseIPAddressList,
	OptionNameServer:                                 parseIPAddressList,
	OptionDomainNameServer:                           parseIPAddressList,
	OptionLogServer:                                  parseIPAddressList,
	OptionCookieServer:                               parseIPAddressList,
	OptionLPRServer:                                  parseIPAddressList,
	OptionImpressServer:                              parseIPAddressList,
	OptionResourceLocationServer:                     parseIPAddressList,
	OptionHostName:                                   parseString,
	OptionBootFileSize:                               parseUint16,
	OptionMeritDumpFile:                              parseString,
	OptionDomainName:                                 parseString,
	OptionSwapServer:                                 parseIPAddress,
	OptionRootPath:                                   parseString,
	OptionExtensionsPath:                             parseString,
	OptionIPForwarding:                               parseBoolean,
	OptionNonLocalSourceRouting:                      parseBoolean,
	OptionPolicyFilter:                               parseIPAndMaskList,
	OptionMaximumDatagramReassemblySize:              parseUint16,
	OptionDefaultIPTTL:                               parseUint8,
	OptionPathMTUAgingTimeout:                        parseUint32,
	OptionPathMTUPlateauTable:                        parseUint16List,
	OptionInterfaceMTU:                               parseUint16,
	OptionAllSubnetsAreLocal:                         parseBoolean,
	OptionBroadcastAddress:                           parseIPAddress,
	OptionPerformMaskDiscovery:                       parseBoolean,
	OptionMaskSupplier:                               parseBoolean,
	OptionPerformRouterDiscovery:                     parseBoolean,
	OptionRouterSolicitationAddress:                  parseIPAddress,
	OptionStaticRoute:                                parseStaticRoutes,
	OptionTrailerEncapsulation:                       parseBoolean,
	OptionARPCacheTimeout:                            parseUint32,
	OptionEthernetEncapsulation:                      parseBoolean,
	OptionTCPDefaultTTL:                              parseUint8,
	OptionTCPKeepaliveInterval:                       parseUint32,
	OptionTCPKeepaliveGarbage:                        parseBoolean,
	OptionNISDomain:                                  parseString,
	OptionNISServers:                                 parseIPAddressList,
	OptionNTPServers:                                 parseIPAddressList,
	OptionVendorSpecificInformation:                  parseString, // Might need a more specific parser
	OptionNetBIOSOverTCPIPNameServer:                 parseIPAddressList,
	OptionNetBIOSOverTCPIPDatagramDistributionServer: parseIPAddressList,
	OptionNetBIOSOverTCPIPNodeType:                   parseUint8,
	OptionNetBIOSOverTCPIPScope:                      parseString,
	OptionXWindowSystemFontServer:                    parseIPAddressList,
	OptionXWindowSystemDisplayManager:                parseIPAddressList,
	OptionIPAddressLeaseTime:                         parseDuration,
	OptionOptionOverload:                             parseUint8,
	OptionDHCPMessageType:                            parseUint8,
	OptionServerIdentifier:                           parseIPAddress,
	OptionMessage:                                    parseString,
	OptionMaximumDHCPMessageSize:                     parseUint16,
	OptionRenewalTimeValue:                           parseDuration,
	OptionRebindingTimeValue:                         parseDuration,
	OptionVendorClassIdentifier:                      parseString,
	OptionNISPlusDomain:                              parseString,
	OptionNISPlusServers:                             parseIPAddressList,
	OptionTFTPServerName:                             parseString,
	OptionBootfileName:                               parseString,
	OptionMobileIPHomeAgent:                          parseIPAddressList,
	OptionSMTPServer:                                 parseIPAddressList,
	OptionPOP3Server:                                 parseIPAddressList,
	OptionNNTPServer:                                 parseIPAddressList,
	OptionDefaultWWWServer:                           parseIPAddressList,
	OptionDefaultFingerServer:                        parseIPAddressList,
	OptionDefaultIRCServer:                           parseIPAddressList,
	OptionStreetTalkServer:                           parseIPAddressList,
	OptionStreetTalkDirectoryAssistanceServer:        parseIPAddressList,
}

// parseIPAddress parses a single IPv4 address
func parseIPAddress(data []byte) (any, error) {
	if len(data) != net.IPv4len {
		return nil, fmt.Errorf("invalid data length for IP address: %d", len(data))
	}
	return net.IP(data), nil
}

// parseIPAddressList parses a list of IPv4 addresses
func parseIPAddressList(data []byte) (any, error) {
	if len(data)%net.IPv4len != 0 {
		return nil, fmt.Errorf("invalid data length for IP address list: %d", len(data))
	}
	var ips []net.IP
	for i := 0; i < len(data); i += net.IPv4len {
		ips = append(ips, net.IP(data[i:i+net.IPv4len]))
	}
	return ips, nil
}

// parseDuration parses a uint32 as a duration in seconds
func parseDuration(data []byte) (any, error) {
	n, err := parseUint32(data)
	if err != nil {
		return nil, err
	}

	return time.Duration(n.(uint32)) * time.Second, nil
}

// parseUint32 parses a 4-byte unsigned integer
func parseUint32(data []byte) (any, error) {
	if len(data) != 4 {
		return nil, fmt.Errorf("invalid data length for uint32: %d", len(data))
	}
	return binary.BigEndian.Uint32(data), nil
}

// parseUint16 parses a 2-byte unsigned integer
func parseUint16(data []byte) (any, error) {
	if len(data) != 2 {
		return nil, fmt.Errorf("invalid data length for uint16: %d", len(data))
	}
	return binary.BigEndian.Uint16(data), nil
}

// parseInt32 parses a 4-byte signed integer
func parseInt32(data []byte) (any, error) {
	if len(data) != 4 {
		return nil, fmt.Errorf("invalid data length for int32: %d", len(data))
	}
	return int32(binary.BigEndian.Uint32(data)), nil
}

// parseUint16List parses a list of uint16 numbers
func parseUint16List(data []byte) (any, error) {
	if len(data)%2 != 0 {
		return nil, fmt.Errorf("invalid data length for uint16 list: %d", len(data))
	}
	var nums []uint16
	for i := 0; i < len(data); i += 2 {
		nums = append(nums, binary.BigEndian.Uint16(data[i:i+2]))
	}
	return nums, nil
}

// parseIPAndMaskList parses a list of IP address and mask pairs
func parseIPAndMaskList(data []byte) (any, error) {
	if len(data)%8 != 0 {
		return nil, fmt.Errorf("invalid data length for IP and mask list: %d", len(data))
	}

	var pairs []IPMaskPair
	for i := 0; i < len(data); i += 8 {
		pairs = append(pairs, IPMaskPair{
			IP:   net.IP(data[i : i+4]),
			Mask: net.IP(data[i+4 : i+8]),
		})
	}
	return pairs, nil
}

// parseStaticRoutes parses a list of static routes
func parseStaticRoutes(data []byte) (any, error) {
	if len(data)%8 != 0 {
		return nil, fmt.Errorf("invalid data length for static routes: %d", len(data))
	}
	var routes []StaticRoute

	for i := 0; i < len(data); i += 8 {
		routes = append(routes, StaticRoute{
			Destination: net.IP(data[i : i+4]),
			Router:      net.IP(data[i+4 : i+8]),
		})
	}
	return routes, nil
}

// parseUint8 parses a single byte unsigned integer
func parseUint8(data []byte) (any, error) {
	if len(data) != 1 {
		return nil, fmt.Errorf("invalid data length for uint8: %d", len(data))
	}
	return data[0], nil
}

// parseString parses a byte slice into a string
func parseString(data []byte) (any, error) {
	return string(data), nil
}

// parseBoolean parses a single byte as a boolean
func parseBoolean(data []byte) (any, error) {
	if len(data) != 1 {
		return nil, fmt.Errorf("invalid data length for boolean: %d", len(data))
	}
	return data[0] != 0, nil
}

// parseDHCPOption parses a DHCP option and returns its formatted value
func parseDHCPOption(opt DHCPOption) (any, error) {
	info, ok := DHCPOptions[opt.Code]
	if !ok {
		return nil, fmt.Errorf("unknown option code: %d", opt.Code)
	}

	if len(opt.Data) < info.MinLength {
		return nil, fmt.Errorf("option %d data too short: %d < %d", opt.Code, len(opt.Data), info.MinLength)
	}

	parser, exists := optionParsers[opt.Code]
	if !exists {
		return fmt.Sprintf("%x", opt.Data), nil
	}

	return parser(opt.Data)
}

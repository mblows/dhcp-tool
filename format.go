package main

import (
	"fmt"
	"net"
	"strings"

	"github.com/gookit/color"
)

// intSliceToString converts an int slice to a string for printing
func intSliceToString(slice any) string {
	switch v := slice.(type) {
	case []uint8:
		return fmt.Sprintf("%v", v)
	case []uint16:
		return fmt.Sprintf("%v", v)
	case []uint32:
		return fmt.Sprintf("%v", v)
	case []int8:
		return fmt.Sprintf("%v", v)
	case []int16:
		return fmt.Sprintf("%v", v)
	case []int32:
		return fmt.Sprintf("%v", v)
	default:
		return ""
	}
}

// formatDHCPOption formats a DHCP option for printing
func formatIPSlice(ips []net.IP) string {
	ipStrings := make([]string, len(ips))
	for i, ip := range ips {
		ipStrings[i] = ip.String()
	}
	return strings.Join(ipStrings, ", ")
}

// formatStaticRoutes formats a list of static routes for printing
func formatStaticRoutes(routes []StaticRoute) string {
	routeStrings := make([]string, len(routes))
	for i, route := range routes {
		routeStrings[i] = fmt.Sprintf("%s via %s", route.Destination, route.Router)
	}
	return strings.Join(routeStrings, ", ")
}

// formatIPMaskList formats a list of IP/mask pairs for printing
func formatIPMaskList(ranges []IPMaskPair) string {
	rangeStrings := make([]string, len(ranges))
	for i, ip := range ranges {
		rangeStrings[i] = fmt.Sprintf("%s/%s", ip.IP, ip.Mask)
	}
	return strings.Join(rangeStrings, ", ")
}

// formatDHCPOption formats a DHCP option for printing
func formatDHCPOption(opt DHCPOption) string {
	info, ok := DHCPOptions[opt.Code]
	if !ok {
		return fmt.Sprintf("Unknown option %d: %x", opt.Code, opt.Data)
	}

	value, err := parseDHCPOption(opt)
	if err != nil {
		return fmt.Sprintf("%s (%d): Error parsing - %v", info.Name, opt.Code, err)
	}

	if value == nil {
		return fmt.Sprintf("%s (%d)", info.Name, opt.Code)
	}

	// Special handling for DHCP Message Type
	if opt.Code == 53 {
		if msgType, ok := value.(uint8); ok {
			if msgName, exists := DHCPMessageTypes[msgType]; exists {
				return fmt.Sprintf("<fg=lightBlue>%s</> [<lightYellow>%d</>]: <bold>%s</> [<lightYellow>%d</>]", info.Name, opt.Code, msgName, msgType)
			}
			return fmt.Sprintf("%s [%d]: Unknown Message Type (%d)", info.Name, opt.Code, msgType)
		}
	}

	// Handle formatting based on the type of value
	switch v := value.(type) {
	case net.IP:
		return color.Sprintf("<fg=lightBlue>%s</> [<lightYellow>%d</>]: %s", info.Name, opt.Code, v)
	case []net.IP:
		return color.Sprintf("<fg=lightBlue>%s</> [<lightYellow>%d</>]: %s", info.Name, opt.Code, formatIPSlice(v))
	case uint8, uint16, uint32, int8, int16, int32:
		return color.Sprintf("<fg=lightBlue>%s</> [<lightYellow>%d</>]: %v", info.Name, opt.Code, v)
	case []uint8, []uint16, []uint32, []int8, []int16, []int32:
		return color.Sprintf("<fg=lightBlue>%s</> [<lightYellow>%d</>]: %s", info.Name, opt.Code, intSliceToString(v))
	case string:
		return color.Sprintf("<fg=lightBlue>%s</> [<lightYellow>%d</>]: %s", info.Name, opt.Code, v)
	case ClientIdentifier:
		return color.Sprintf("<fg=lightBlue>%s</> [<lightYellow>%d</>]: Hardware type: %d, Address: %s", info.Name, opt.Code, v.HardwareType, v.HardwareAddr)
	case []StaticRoute:
		return color.Sprintf("<fg=lightBlue>%s</> [<lightYellow>%d</>]: %s", info.Name, opt.Code, formatStaticRoutes(v))
	case []IPMaskPair:
		return color.Sprintf("<fg=lightBlue>%s</> [<lightYellow>%d</>]: %s", info.Name, opt.Code, formatIPMaskList(v))
	default:
		return color.Sprintf("<fg=lightBlue>%s</> [<lightYellow>%d</>]: %v", info.Name, opt.Code, v)
	}
}

// printDHCPOptions prints all DHCP options in a readable format
func printDHCPOptions(options []DHCPOption) {
	fmt.Println("Server DHCP Options:")
	for _, opt := range options {
		color.Printf("  %s\n", formatDHCPOption(opt))
	}
}

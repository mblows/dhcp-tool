package main

import (
	"bytes"
	"encoding/binary"
	"flag"
	"fmt"
	"math/rand/v2"
	"net"
	"os"
	"slices"
	"strconv"
	"strings"
	"time"

	"github.com/gookit/color"
	"golang.org/x/net/ipv4"
)

// printInterfaces prints all available network interfaces on the system, along
// with their MAC addresses and status.
func printInterfaces() {
	interfaces, err := net.Interfaces()
	if err != nil {
		color.LightRed.Printf("Error getting network interfaces: %v\n", err)
		return
	}

	fmt.Printf("%-10s %-17s %s\n", "Name", "MAC Address", "Status")
	for _, iface := range interfaces {
		status := "down"
		if iface.Flags&net.FlagUp != 0 && iface.Flags&net.FlagRunning != 0 {
			status = "up"
		}
		fmt.Printf("%-10s %-17s %s\n", iface.Name, iface.HardwareAddr, status)
	}
}

// createRandomHwAddr generates a random MAC address with the local bit set.
func createRandomHwAddr() net.HardwareAddr {
	mac := make(net.HardwareAddr, 6)
	for i := range mac {
		mac[i] = byte(rand.UintN(256))
	}

	mac[0] |= 2 // Set the local bit

	return mac
}

// createDHCPMessage creates a DHCP message with the specified type, transaction ID, and MAC address.
func createDHCPMessage(messageType uint8, mac net.HardwareAddr, xid uint32, options []uint8) *DHCPMessage {
	msg := newDHCPMessage()

	msg.OpCode = 1           // BootRequest
	msg.HType = 1            // Ethernet
	msg.HLen = 6             // MAC address length
	msg.Secs = 0             // 0 seconds elapsed
	msg.Xid = xid            // Transaction ID
	msg.Flags = 0x8000       // Set broadcast flag
	msg.Cookie = magicCookie // Magic cookie
	copy(msg.ChAddr[:], mac) // Client hardware address

	// Set client identifier
	clientID := ClientIdentifier{HardwareType: 1, HardwareAddr: mac}
	msg.setOption(OptionClientIdentifier, clientID.Marshal())

	// Set hostname if available
	if hostname, err := os.Hostname(); err == nil {
		msg.setOption(OptionHostName, []byte(hostname))
	}

	// Set message type and options
	msg.setOption(OptionDHCPMessageType, []byte{messageType})
	msg.setOption(OptionParameterRequestList, options)

	return msg
}

// sendDHCPMessage sends a DHCP message over the raw connection.
func sendDHCPMessage(conn *ipv4.RawConn, msg *DHCPMessage, destIP net.IP) error {
	data, err := msg.Marshal()
	if err != nil {
		return fmt.Errorf("error marshalling message: %w", err)
	}

	// Construct UDP header
	var udpHeader bytes.Buffer
	binary.Write(&udpHeader, binary.BigEndian, DHCPClientPort)      // Source port
	binary.Write(&udpHeader, binary.BigEndian, DHCPServerPort)      // Destination port
	binary.Write(&udpHeader, binary.BigEndian, uint16(8+len(data))) // Length
	binary.Write(&udpHeader, binary.BigEndian, uint16(0))           // Checksum (0, set by kernel)
	packet := append(udpHeader.Bytes(), data...)

	// Construct IPv4 header
	header := &ipv4.Header{
		Version:  4,                // IPv4
		Len:      ipv4.HeaderLen,   // 20 bytes
		TotalLen: 20 + len(packet), // Total length
		TTL:      64,               // Time to live (Unix)
		Protocol: 17,               // UDP
		Dst:      destIP,           // Broadcast address
		Src:      net.IPv4zero,     // Source address (0.0.0.0)
	}

	// Send the packet
	if err := conn.WriteTo(header, packet, nil); err != nil {
		return fmt.Errorf("error sending message: %w", err)
	}

	return nil
}

// receiveDHCPMessage listens for a specific type of DHCP message with the expected transaction ID.
func receiveDHCPMessage(conn *ipv4.RawConn, expectedType uint8, xid uint32) (*DHCPMessage, error) {
	buffer := make([]byte, 1500) // Maximum size of an Ethernet frame
	deadline := time.Now().Add(timeout)

	// Set read timeout
	if err := conn.SetReadDeadline(deadline); err != nil {
		return nil, fmt.Errorf("error setting read deadline: %v", err)
	}

	// Loop and read packets until we get a DHCP message
	for {
		_, payload, _, err := conn.ReadFrom(buffer)
		if err != nil {
			if netErr, ok := err.(net.Error); ok && netErr.Timeout() {
				return nil, fmt.Errorf("timeout waiting for DHCP message")
			}
			return nil, err
		}

		// Ignore packets for ports other than DHCP client port
		if len(payload) < 8 || binary.BigEndian.Uint16(payload[2:4]) != DHCPClientPort {
			continue
		}

		// Extract DHCP message from UDP payload
		dhcpData := payload[8:]

		// Unmarshal the DHCP message
		var msg DHCPMessage
		if err := msg.Unmarshal(dhcpData); err != nil {
			continue
		}

		// Check if it's a DHCP message by checking the magic cookie
		if msg.Cookie != magicCookie {
			continue
		}

		// Check if the transaction ID matches
		if msg.Xid != xid {
			continue
		}

		// Check the message type
		msgTypeOption := msg.GetOption(OptionDHCPMessageType)
		if len(msgTypeOption) == 0 {
			continue
		}

		msgType := msgTypeOption[0]
		if msgType == DHCPNAK {
			return nil, fmt.Errorf("server rejected our request")
		}

		// If the message type is not what we're looking for, skip it
		if msgType != expectedType {
			continue
		}

		return &msg, nil
	}
}

func main() {
	var macAddr net.HardwareAddr
	listInterfaces := flag.Bool("l", false, "List available network interfaces")
	ifaceName := flag.String("i", "", "Network interface to use")
	options := flag.String("o", "", "Comma-separated list of additional DHCP options to request")
	mac := flag.String("m", "", "MAC address to use for the DHCP request")
	randomMac := flag.Bool("r", false, "Generate a random MAC address for the DHCP request")
	nReq := flag.Int("n", 1, "Number of DHCP requests to send")
	// hostname := flag.String("h", "", "Hostname to include in the DHCP request")
	flag.Parse()

	if os.Geteuid() != 0 {
		color.LightRed.Println("This program must be run as root")
		os.Exit(1)
	}

	if *listInterfaces {
		printInterfaces()
		return
	}

	if *ifaceName == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	// Default options to request
	optionsList := []uint8{
		OptionSubnetMask,
		OptionRouter,
		OptionDomainNameServer,
		OptionBroadcastAddress,
		OptionIPAddressLeaseTime,
		OptionDomainName,
	}

	// Parse additional options requested by the user
	for _, optStr := range strings.Split(*options, ",") {
		if optStr == "" {
			continue
		}

		optUint, err := strconv.ParseUint(optStr, 10, 8)
		if err != nil || optUint < 1 || optUint > 254 {
			color.LightRed.Printf("Invalid DHCP option: %s\n", optStr)
			os.Exit(1)
		}

		opt := uint8(optUint)
		if !slices.Contains(optionsList, opt) {
			optionsList = append(optionsList, opt)
		}
	}

	// Parse the MAC address if the user provided one
	if *mac != "" {
		if *randomMac {
			color.LightRed.Println("Cannot specify both -m and -r")
			os.Exit(1)
		}

		var err error
		macAddr, err = net.ParseMAC(*mac)
		if err != nil {
			color.LightRed.Printf("Invalid MAC address %s: %v\n", *mac, err)
			os.Exit(1)
		}
	}

	// Get the network interface by name
	iface, err := net.InterfaceByName(*ifaceName)
	if err != nil {
		color.LightRed.Printf("Error getting network interface %s: %v\n", *ifaceName, err)
		os.Exit(1)
	}

	// Check if the interface is up
	if iface.Flags&net.FlagUp == 0 || iface.Flags&net.FlagRunning == 0 {
		color.LightRed.Printf("Interface %s is down\n", *ifaceName)
		os.Exit(1)
	}

	// Create a raw socket
	conn, err := createRawSocket(iface)
	if err != nil {
		color.LightRed.Printf("Error creating raw socket: %v\n", err)
		os.Exit(1)
	}
	defer conn.Close()

	for i := range *nReq {
		if *nReq > 1 {
			fmt.Printf("Request %d of %d\n", i+1, *nReq)
		}

		// Generate a new transaction ID for each request
		xid := rand.Uint32()

		// Determine the MAC address to use
		if *randomMac {
			macAddr = createRandomHwAddr()
		} else if macAddr == nil {
			macAddr = iface.HardwareAddr
		}

		// Send DHCPDISCOVER
		color.Printf("Broadcasting DHCP discovery message on <fg=lightYellow>%s</> (%v)...\n", *ifaceName, macAddr)
		discoverMsg := createDHCPMessage(DHCPDISCOVER, macAddr, xid, optionsList)
		if err := sendDHCPMessage(conn, discoverMsg, net.IPv4bcast); err != nil {
			color.LightRed.Printf("Error sending DHCP discovery message: %v\n", err)
			os.Exit(1)
		}

		// Listen for DHCPOFFER
		offerMsg, err := receiveDHCPMessage(conn, DHCPOFFER, xid)
		if err != nil {
			color.LightRed.Printf("Failed to receive DHCP offer: %v\n", err)
			continue
		}

		// Extract offered IP and options
		offeredIP := net.IP(offerMsg.YiAddr[:])
		sourceIP := net.IP(offerMsg.SiAddr[:])
		color.Printf("Received DHCP offer of <fg=lightGreen>%v</> from <fg=magenta>%v</>\n", offeredIP, sourceIP)
		printDHCPOptions(offerMsg.Options)

		// Send DHCPREQUEST back to the server that offered the IP
		color.Printf("\nAsking <fg=magenta>%v</> to lease <fg=lightGreen>%v</>...\n", sourceIP, offeredIP)
		requestMsg := createDHCPMessage(DHCPREQUEST, macAddr, xid, optionsList)
		requestMsg.setOption(OptionRequestedIPAddress, offeredIP)
		requestMsg.setOption(OptionServerIdentifier, sourceIP)
		if err := sendDHCPMessage(conn, requestMsg, net.IPv4bcast); err != nil {
			color.LightRed.Printf("Error sending DHCP request: %v\n", err)
			os.Exit(1)
		}

		// Listen for DHCPACK
		ackMsg, err := receiveDHCPMessage(conn, DHCPACK, xid)
		if err != nil {
			color.LightRed.Printf("Error receiving DHCP acknowledgement: %v\n", err)
			continue
		}

		// Confirm IP and print final options
		confirmedIP := net.IP(ackMsg.YiAddr[:])
		color.Printf("Received DHCP acknowledgement. Confirmed IP: <fg=lightGreen;op=bold;op=underscore>%s</>\n", confirmedIP)
		printDHCPOptions(ackMsg.Options)
		fmt.Println()
	}
}

package main

import "time"

const (
	// Timeout for DHCP operations
	timeout = 3 * time.Second

	// DHCP Ports
	DHCPClientPort uint16 = 68
	DHCPServerPort uint16 = 67

	// Magic Cookie used in DHCP messages
	magicCookie = uint32(0x63825363)

	// DHCP Message Types
	DHCPDISCOVER         uint8 = 1
	DHCPOFFER            uint8 = 2
	DHCPREQUEST          uint8 = 3
	DHCPDECLINE          uint8 = 4
	DHCPACK              uint8 = 5
	DHCPNAK              uint8 = 6
	DHCPRELEASE          uint8 = 7
	DHCPINFORM           uint8 = 8
	DHCPFORCERENEW       uint8 = 9
	DHCPLEASEQUERY       uint8 = 10
	DHCPLEASEUNASSIGNED  uint8 = 11
	DHCPLEASEUNKNOWN     uint8 = 12
	DHCPLEASEACTIVE      uint8 = 13
	DHCPBULKLEASEQUERY   uint8 = 14
	DHCPLEASEQUERYDONE   uint8 = 15
	DHCPACTIVELEASEQUERY uint8 = 16
	DHCPLEASEQUERYSTATUS uint8 = 17
	DHCPTLS              uint8 = 18

	// RFC 2132 DHCP Options
	OptionPad                                        = 0   // Padding
	OptionSubnetMask                                 = 1   // Subnet mask
	OptionTimeOffset                                 = 2   // Time offset
	OptionRouter                                     = 3   // Router
	OptionTimeServer                                 = 4   // Time server
	OptionNameServer                                 = 5   // Name server
	OptionDomainNameServer                           = 6   // Domain name server
	OptionLogServer                                  = 7   // Log server
	OptionCookieServer                               = 8   // Cookie server
	OptionLPRServer                                  = 9   // LPR server
	OptionImpressServer                              = 10  // Impress server
	OptionResourceLocationServer                     = 11  // Resource location server
	OptionHostName                                   = 12  // Host name
	OptionBootFileSize                               = 13  // Boot file size
	OptionMeritDumpFile                              = 14  // Merit dump file
	OptionDomainName                                 = 15  // Domain name
	OptionSwapServer                                 = 16  // Swap server
	OptionRootPath                                   = 17  // Root path
	OptionExtensionsPath                             = 18  // Extensions path
	OptionIPForwarding                               = 19  // IP forwarding
	OptionNonLocalSourceRouting                      = 20  // Non-local source routing
	OptionPolicyFilter                               = 21  // Policy filter
	OptionMaximumDatagramReassemblySize              = 22  // Maximum datagram reassembly size
	OptionDefaultIPTTL                               = 23  // Default IP TTL
	OptionPathMTUAgingTimeout                        = 24  // Path MTU aging timeout
	OptionPathMTUPlateauTable                        = 25  // Path MTU plateau table
	OptionInterfaceMTU                               = 26  // Interface MTU
	OptionAllSubnetsAreLocal                         = 27  // All subnets are local
	OptionBroadcastAddress                           = 28  // Broadcast address
	OptionPerformMaskDiscovery                       = 29  // Perform mask discovery
	OptionMaskSupplier                               = 30  // Mask supplier
	OptionPerformRouterDiscovery                     = 31  // Perform router discovery
	OptionRouterSolicitationAddress                  = 32  // Router solicitation address
	OptionStaticRoute                                = 33  // Static route
	OptionTrailerEncapsulation                       = 34  // Trailer encapsulation
	OptionARPCacheTimeout                            = 35  // ARP cache timeout
	OptionEthernetEncapsulation                      = 36  // Ethernet encapsulation
	OptionTCPDefaultTTL                              = 37  // TCP default TTL
	OptionTCPKeepaliveInterval                       = 38  // TCP keepalive interval
	OptionTCPKeepaliveGarbage                        = 39  // TCP keepalive garbage
	OptionNISDomain                                  = 40  // NIS domain
	OptionNISServers                                 = 41  // NIS servers
	OptionNTPServers                                 = 42  // NTP servers
	OptionVendorSpecificInformation                  = 43  // Vendor specific information
	OptionNetBIOSOverTCPIPNameServer                 = 44  // NetBIOS over TCP/IP name server
	OptionNetBIOSOverTCPIPDatagramDistributionServer = 45  // NetBIOS over TCP/IP datagram distribution server
	OptionNetBIOSOverTCPIPNodeType                   = 46  // NetBIOS over TCP/IP node type
	OptionNetBIOSOverTCPIPScope                      = 47  // NetBIOS over TCP/IP scope
	OptionXWindowSystemFontServer                    = 48  // X Window System font server
	OptionXWindowSystemDisplayManager                = 49  // X Window System display manager
	OptionRequestedIPAddress                         = 50  // Requested IP address
	OptionIPAddressLeaseTime                         = 51  // IP address lease time
	OptionOptionOverload                             = 52  // Option overload
	OptionDHCPMessageType                            = 53  // DHCP message type
	OptionServerIdentifier                           = 54  // Server identifier
	OptionParameterRequestList                       = 55  // Parameter request list
	OptionMessage                                    = 56  // Message
	OptionMaximumDHCPMessageSize                     = 57  // Maximum DHCP message size
	OptionRenewalTimeValue                           = 58  // Renewal time value
	OptionRebindingTimeValue                         = 59  // Rebinding time value
	OptionVendorClassIdentifier                      = 60  // Vendor class identifier
	OptionClientIdentifier                           = 61  // Client identifier
	OptionNISPlusDomain                              = 64  // NIS+ domain
	OptionNISPlusServers                             = 65  // NIS+ servers
	OptionTFTPServerName                             = 66  // TFTP server name
	OptionBootfileName                               = 67  // Boot file name
	OptionMobileIPHomeAgent                          = 68  // Mobile IP home agent
	OptionSMTPServer                                 = 69  // SMTP server
	OptionPOP3Server                                 = 70  // POP3 server
	OptionNNTPServer                                 = 71  // NNTP server
	OptionDefaultWWWServer                           = 72  // Default WWW server
	OptionDefaultFingerServer                        = 73  // Default finger server
	OptionDefaultIRCServer                           = 74  // Default IRC server
	OptionStreetTalkServer                           = 75  // StreetTalk server
	OptionStreetTalkDirectoryAssistanceServer        = 76  // StreetTalk directory assistance server
	OptionEnd                                        = 255 // End
)

// DHCP Message Types Mapping
var DHCPMessageTypes = map[uint8]string{
	DHCPDISCOVER:         "DHCPDISCOVER",
	DHCPOFFER:            "DHCPOFFER",
	DHCPREQUEST:          "DHCPREQUEST",
	DHCPDECLINE:          "DHCPDECLINE",
	DHCPACK:              "DHCPACK",
	DHCPNAK:              "DHCPNAK",
	DHCPRELEASE:          "DHCPRELEASE",
	DHCPINFORM:           "DHCPINFORM",
	DHCPFORCERENEW:       "DHCPFORCERENEW",
	DHCPLEASEQUERY:       "DHCPLEASEQUERY",
	DHCPLEASEUNASSIGNED:  "DHCPLEASEUNASSIGNED",
	DHCPLEASEUNKNOWN:     "DHCPLEASEUNKNOWN",
	DHCPLEASEACTIVE:      "DHCPLEASEACTIVE",
	DHCPBULKLEASEQUERY:   "DHCPBULKLEASEQUERY",
	DHCPLEASEQUERYDONE:   "DHCPLEASEQUERYDONE",
	DHCPACTIVELEASEQUERY: "DHCPACTIVELEASEQUERY",
	DHCPLEASEQUERYSTATUS: "DHCPLEASEQUERYSTATUS",
	DHCPTLS:              "DHCPTLS",
}

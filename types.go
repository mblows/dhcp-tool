package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"net"
)

// DHCPHeader represents the header of a DHCP message
// See https://www.rfc-editor.org/rfc/rfc2131.txt for DHCP message format
type DHCPHeader struct {
	OpCode byte      // 1 = BootRequest, 2 = BootReply
	HType  byte      // Hardware address type
	HLen   byte      // Hardware address length
	Hops   byte      // Used by relay agents
	Xid    uint32    // Transaction ID
	Secs   uint16    // Seconds elapsed since client started trying to boot
	Flags  uint16    // Flags
	CiAddr [4]byte   // Client IP address
	YiAddr [4]byte   // Your IP address
	SiAddr [4]byte   // Server IP address
	GiAddr [4]byte   // Gateway IP address
	ChAddr [16]byte  // Client hardware address
	SName  [64]byte  // Server host name
	File   [128]byte // Boot file name
	Cookie uint32    // Magic cookie
}

// DHCPOption represents a single DHCP option
type DHCPOption struct {
	Code uint8  // Option code
	Data []byte // Option data
}

// DHCPMessage represents a complete DHCP message
type DHCPMessage struct {
	DHCPHeader              // DHCP header
	Options    []DHCPOption // Slice of options in the order they appear in the message
}

// ClientIdentifier represents a DHCP client identifier
type ClientIdentifier struct {
	HardwareType byte
	HardwareAddr net.HardwareAddr
}

// DHCPOptionInfo stores information about each DHCP option
type DHCPOptionInfo struct {
	Name        string // Option name
	MinLength   int    // Minimum length of the option data in octets
	Description string // Description of the option
}

// StaticRoute represents a static route in a DHCP message
type StaticRoute struct {
	Destination net.IP
	Router      net.IP
}

// IPMaskPair represents a pair of IP address and subnet mask
type IPMaskPair struct {
	IP   net.IP
	Mask net.IP
}

// newDHCPMessage creates a new DHCPMessage with initialised maps
func newDHCPMessage() *DHCPMessage {
	return &DHCPMessage{}
}

// Marshal serialises the ClientIdentifier to a byte slice
func (c *ClientIdentifier) Marshal() []byte {
	buf := new(bytes.Buffer)
	buf.WriteByte(c.HardwareType)
	buf.Write(c.HardwareAddr)
	return buf.Bytes()
}

// setOption adds or updates a DHCP option in the message
func (d *DHCPMessage) setOption(code uint8, data []byte) {
	for i, opt := range d.Options {
		if opt.Code == code {
			d.Options[i].Data = data
			return
		}
	}
	d.Options = append(d.Options, DHCPOption{Code: code, Data: data})
}

// GetOption retrieves a DHCP option from the message
func (d *DHCPMessage) GetOption(code uint8) []byte {
	for _, opt := range d.Options {
		if opt.Code == code {
			return opt.Data
		}
	}
	return nil
}

// Marshal serialises the DHCP message to a byte slice
func (d *DHCPMessage) Marshal() ([]byte, error) {
	buf := new(bytes.Buffer)

	// Write the DHCP header
	if err := binary.Write(buf, binary.BigEndian, &d.DHCPHeader); err != nil {
		return nil, err
	}

	// Write the DHCP options
	for _, opt := range d.Options {
		buf.WriteByte(opt.Code)
		if opt.Code == OptionPad {
			continue
		}
		buf.WriteByte(uint8(len(opt.Data)))
		buf.Write(opt.Data)
	}

	// Add the end option when finished
	buf.WriteByte(OptionEnd)

	return buf.Bytes(), nil
}

// Unmarshal deserializes the DHCP message from a byte slice
func (d *DHCPMessage) Unmarshal(data []byte) error {
	reader := bytes.NewReader(data)

	// Read the DHCP header
	if err := binary.Read(reader, binary.BigEndian, &d.DHCPHeader); err != nil {
		return err
	}

	// Check the magic cookie, telling us this is a DHCP message
	if d.Cookie != magicCookie {
		return fmt.Errorf("invalid magic cookie: expected %x, got %x", magicCookie, d.Cookie)
	}

	d.Options = []DHCPOption{}
	for {
		code, err := reader.ReadByte()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		if code == OptionPad {
			continue
		}
		if code == OptionEnd {
			break
		}
		length, err := reader.ReadByte()
		if err != nil {
			return err
		}
		data := make([]byte, length)
		if _, err = io.ReadFull(reader, data); err != nil {
			return err
		}
		d.Options = append(d.Options, DHCPOption{Code: code, Data: data})
	}

	return nil
}
